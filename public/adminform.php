<form action="admin.php"
        method="POST">
<input type="hidden" name="uid" value="<?php print $row['id_client']; ?>" />
<label>
    Имя<br />
    <input name="name" value="<?php print $row['name']; ?>"/>
</label><br />

<label>
    E-mail:<br />
    <input name="email" value="<?php print $row['email']; ?>" type="email" />
</label><br />

<label>
    Дата рождения:<br />
    <input name="birthday" value="<?php print $row['birthday']; ?>" type="date" />
</label><br />

Пол:<br />
<label><input type="radio"
    name="gender" value="M" <?php if ($row['gender'] == 'M') {print 'checked';} ?>/>
    мужской
</label>
<label><input type="radio"
    name="gender" value="F" <?php if ($row['gender'] == 'F') {print 'checked';} ?> />
    женский
</label>
<br />

Количество конечностей:
<label><input type="radio" <?php if ($row['limbs'] == '1') {print 'checked';} ?>
    name="limbs" value="1" />
    3
</label>
<label><input type="radio" <?php if ($row['limbs'] == '2') {print 'checked';} ?>
    name="limbs" value="2" />
    4
</label>
<label><input type="radio" <?php if ($row['limbs'] == '3') {print 'checked';} ?>
    name="limbs" value="3" />
    5
</label>

<label>
    Сверхспособности:
    <select name="abilitiess[]"
    multiple="multiple">
    <option value="1" <?php if ($abilitiess[1]) {print 'selected';} ?>>Чтение мыслей</option>
    <option value="2" <?php if ($abilitiess[2]) {print 'selected';} ?>>Перерождение</option>
    <option value="3" <?php if ($abilitiess[3]) {print 'selected';} ?>>Управление солнечным светом</option>
    <option value="4" <?php if ($abilitiess[4]) {print 'selected';} ?>>Заговаривание воды</option>
    </select>
</label>

<label>
    Биография:<br />
    <textarea name="biography"><?php print $row['biography']; ?></textarea>
</label><br />

<input id="submit" type="submit" value="Изменить" name="update" />
<input id="submit" type="submit" value="Удалить" name="delete"/>
</form>
