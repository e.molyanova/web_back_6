<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>web_back_6</title>
  <!--  <script defer src="https://code.jquery.com/jquery-1.11.0.min.js"></script>-->
 <!--   <script defer src="main.js"></script>  -->
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="облако.png" width="200" height="50"  type="image/x-icon">
  </head>
<body>
  <div class="main">
<?php
  if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) || 
    !empty($_GET['logout'])) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Enter login and password"');
    if (!empty($_GET['logout']))
      header('Location: admin.php');
    print('<h1>401 Нужно авторизоваться</h1></div></body>');
    exit();
  }

  $user = 'u40943';
  $pass = '4137756';
  $db = new PDO('mysql:host=localhost;dbname=u40943', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  $login = trim($_SERVER['PHP_AUTH_USER']);
  $pass_hash = substr(hash("sha256", trim($_SERVER['PHP_AUTH_PW'])), 0, 20);
  $stmtCheck = $db->prepare('SELECT admin_pass_hash FROM admin_login_data WHERE admin_login = ?');
  $stmtCheck->execute([$login]);
  $row = $stmtCheck->fetch(PDO::FETCH_ASSOC);
  if ($row == false || $row['admin_pass_hash'] != $pass_hash) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Invalid login or password"');
    print('<h1>401 Неверный логин или пароль</h1>');
    exit();
  }
?>
    <section>
        <h2>Администрирование</h2>
        <a href="?logout=1">Выйти</a>
    </section>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // пароль qwerty
  
  $stmtCount = $db->prepare('SELECT name_ability, count(fa.id_client) AS amount FROM capabilities AS ab LEFT JOIN clients_capabilities AS fa ON ab.id_ability = fa.id_ability GROUP BY ab.id_ability');
  $stmtCount->execute();
  print('<section>');
  while($row = $stmtCount->fetch(PDO::FETCH_ASSOC)) {
      print('<b>' . $row['name_ability'] . '</b>: ' . $row['amount'] . '<br/>');
  }
  print('</section>');

  $stmt1 = $db->prepare('SELECT id_client, name, email, birthday, gender, limbs, biography, login FROM forma');
  $stmt2 = $db->prepare('SELECT id_ability FROM clients_capabilities WHERE id_client = ?');
  $stmt1->execute();

  while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      print('<section>');
      print('<h2>' . $row['login'] . '</h2>');
      $superpowers = [false, false, false];
      $stmt2->execute([$row['id_client']]);
      while ($superrow = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $superpowers[$superrow['id_ability']] = true;
      }
      foreach ($row as $key => $value)
        if (is_string($value))
          $row[$key] = strip_tags($value);
      include('adminform.php');
      print('</section>');
  }
} else {
  if (array_key_exists('delete', $_POST)) {
    $user = 'u40943';
    $pass = '4137756';
    $db = new PDO('mysql:host=localhost;dbname=u40943', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('DELETE FROM clients_capabilities WHERE id_client = ?');
    $stmt1->execute([$_POST['uid']]);
    $stmt2 = $db->prepare('DELETE FROM forma WHERE id_client = ?');
    $stmt2->execute([$_POST['uid']]);
    header('Location: admin.php');
    exit();
  }

  $trimmedPost = [];
  foreach ($_POST as $key => $value)
    if (is_string($value))
      $trimmedPost[$key] = trim($value);
    else
      $trimmedPost[$key] = $value;

  if (empty($trimmedPost['name'])) {
    $hasErrors = TRUE;
  }
  $values['name'] = strip_tags($trimmedPost['name']);

  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
    $hasErrors = TRUE;
  }
  $values['email'] = strip_tags($trimmedPost['email']);

  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
    $hasErrors = TRUE;
  }
  $values['birthday'] = strip_tags($trimmedPost['birthday']);

  if (!preg_match('/^[MFO]$/', $trimmedPost['gender'])) {
    $hasErrors = TRUE;
  }
  $values['gender'] = strip_tags($trimmedPost['gender']);

  if (!preg_match('/^[1-3]$/', $trimmedPost['limbs'])) {
    $hasErrors = TRUE;
  }
  $values['limbs'] = strip_tags($trimmedPost['limbs']);

  foreach (['1', '2', '3', '4'] as $value) {
    $values['abilitiess'][$value] = FALSE;
  }
  if (array_key_exists('abilitiess', $trimmedPost)) {
    foreach ($trimmedPost['abilitiess'] as $value) {
      if (!preg_match('/[1-4]/', $value)) {
        $hasErrors = TRUE;
      }
      $values['abilitiess'][$value] = TRUE;
    }
  }
  $values['biography'] = strip_tags($trimmedPost['biography']);
  

  if ($hasErrors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: admin.php');
    exit();
  }

  $user = 'u40943';
  $pass = '4137756';
  $db = new PDO('mysql:host=localhost;dbname=u40943', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('UPDATE forma SET name=?, email=?, birthday=?, gender=?, limbs=?, biography=? WHERE id_client = ?');
  $stmt1->execute([$values['name'], $values['email'], $values['birthday'], $values['gender'], $values['limbs'], $values['biography'], $_POST['uid']]);

  $stmt2 = $db->prepare('DELETE FROM clients_capabilities WHERE id_client = ?');
  $stmt2->execute([$_POST['uid']]);

  $stmt3 = $db->prepare("INSERT INTO clients_capabilities SET id_client = ?, id_ability = ?");
  foreach ($trimmedPost['abilitiess'] as $s)
    $stmt3 -> execute([$_POST['uid'], $s]);

  header('Location: admin.php');
  exit();
}

?>
</div>
</body>

